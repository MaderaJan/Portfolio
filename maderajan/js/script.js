//********************************************
//****************SPALSH FOR 2 SEC************
//********************************************
var timeleft = 1;
var downloadTimer = setInterval(function(){
  timeleft--;
  if(timeleft <= 0){
    clearInterval(downloadTimer);
   	var load_screen = document.getElementById("load_screen");
  	load_screen.className += "moveToBottom";
  }
},1000);

//********************************************
//****************DRAW RECTS******************
//********************************************
var body = document.body;
var html = document.documentElement;

var fullHeight = Math.max(body.scrollHeight, body.offsetHeight, html.clientHeight, html.scrollHeight, html.offsetHeight);
var fullWidth = window.innerWidth-25;

var canvas = document.getElementById('myCanvas');
canvas.width = fullWidth;
canvas.height = fullHeight;
var context = canvas.getContext('2d');

var numberOfSquers = Math.floor((Math.random() * 20 ) + 10);
var radius = 10;
for(i = 0; i < numberOfSquers; i++){
	var x = Math.floor((Math.random() * fullWidth) + 1);
	var y = Math.floor((Math.random() * fullHeight) + 1);
	if(i < 2){
		var a = Math.floor((Math.random() * radius) + 10);
	}else if(i < 5){
		var a = Math.floor((Math.random() * radius) + 10);
	}else if(i < 8){
		var a = Math.floor((Math.random() * radius) + 10);
	}else{
		var a = Math.floor((Math.random() * radius) + 10);
	}
	radius = radius + 3;


	var b = Math.floor((Math.random() * 30) + 10);

	context.beginPath();
	//context.rect(x, y, a, b);
	context.arc(x, y, a, 0,2*Math.PI);
	context.fillStyle = 'black';
	context.shadowColor = '#999';
	context.shadowBlur = 3;
	context.shadowOffsetX = 3;
	context.shadowOffsetY = 3;
	context.fill();
}

//********************************************
//****************SMOOTH SCOLL****************
//********************************************
